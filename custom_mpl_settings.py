def load_mpl_rc():
    import matplotlib as mpl
    from matplotlib import cm
    from cycler import cycler

    viridis = cm.get_cmap("viridis", 12)

    default_cycler = cycler(
        linestyle=["-", "--", ":", "-.", "-", "--", ":", "-."]
    ) + cycler(
        color=[
            (0.9, 0.6, 0),
            (0.35, 0.7, 0.9),
            (0, 0.6, 0.5),
            (0, 0, 0),
            (0, 0.45, 0.7),
            (0.8, 0.6, 0.7),
            (0.8, 0.4, 0),
            (0.95, 0.9, 0.25),
        ]
    )

    viridis_cycler = cycler(color=[viridis(0.7), viridis(0.4), viridis(0.1)])

    # https://matplotlib.org/users/customizing.html
    mpl.rc(
        "axes",
        labelweight="normal",
        linewidth=2,
        labelsize=20,
        grid=True,
        titlesize=20,
        prop_cycle=default_cycler,
    )

    mpl.rc("font", size=20)

    mpl.rc("savefig", dpi=200)

    mpl.rc("lines", linewidth=2, color="g", markersize=5)

    mpl.rc("scatter", marker="o")

    mpl.rc(
        "ytick",
        **{
            "labelsize": 15,
            "major.pad": 2,
            "color": "k",
            "left": True,
            "right": True,
            "major.size": 10,
            "minor.size": 6,
            "minor.visible": True,
            "direction": "inout",
        }
    )

    mpl.rc(
        "xtick",
        **{
            "labelsize": 15,
            "major.pad": 2,
            "top": True,
            "bottom": True,
            "major.size": 10,
            "minor.size": 6,
            "minor.visible": True,
            "direction": "inout",
        }
    )

    mpl.rc(
        "contour",
        negative_linestyle="solid",
    )

    mpl.rc("figure", figsize=[16, 9], titlesize=20, dpi=80)

    mpl.rc(
        "legend",
        fontsize=15,
        handlelength=2,
        loc="best",
        fancybox=False,
        numpoints=2,
        framealpha=None,
        scatterpoints=3,
        edgecolor="inherit",
    )

    mpl.rc("grid", color="b0b0b0", alpha=0.5)

    mpl.rc("image", cmap="viridis")

    mpl.rc("savefig", dpi=200, format="pdf", bbox="tight", pad_inches=0.2)

    mpl.rc("figure.subplot", hspace=0.05, wspace=0.05)


def print_mpl_rc():
    """
    Function that prints the current mpl rc params
    """
    import matplotlib as mpl

    print(mpl.rc_params())


#
LINESTYLE_TUPLE = [
    ("solid", "solid"),  # Same as (0, ()) or '-'
    ("dotted", "dotted"),  # Same as (0, (1, 1)) or '.'
    ("dashed", "dashed"),  # Same as '--'
    ("dashdot", "dashdot"),  # Same as '-.'
    # ('loosely dotted',        (0, (1, 10))),
    # ('dotted',                (0, (1, 1))),
    ("densely dotted", (0, (1, 1))),
    ("loosely dashed", (0, (5, 10))),
    # ('dashed',                (0, (5, 5))),
    ("densely dashed", (0, (5, 1))),
    ("loosely dashdotted", (0, (3, 10, 1, 10))),
    # ('dashdotted',            (0, (3, 5, 1, 5))),
    ("densely dashdotted", (0, (3, 1, 1, 1))),
    ("dashdotdotted", (0, (3, 5, 1, 5, 1, 5))),
    ("loosely dashdotdotted", (0, (3, 10, 1, 10, 1, 10))),
    ("densely dashdotdotted", (0, (3, 1, 1, 1, 1, 1))),
    ("solid", "solid"),  # Same as (0, ()) or '-'
    ("dotted", "dotted"),  # Same as (0, (1, 1)) or '.'
    ("dashed", "dashed"),  # Same as '--'
    ("dashdot", "dashdot"),  # Same as '-.'
]
